# [Swarm Simulation Trainer](https://markomilanovic.gitlab.io/swarmsim-trainer/)

> Trainer for [swarmsim.com](https://www.swarmsim.com/), for all the ~~cheaters~~ impatient people out there.

[![pipeline status](https://gitlab.com/markomilanovic/swarmsim-trainer/badges/main/pipeline.svg)](https://gitlab.com/markomilanovic/swarmsim-trainer/-/commits/main)

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn dev
```

### Compiles and minifies for production

```
yarn build
```

### Lints and fixes files

```
yarn lint
```

## License

MIT &copy; Marko Milanovic <markomilanovic@zoho.com>
